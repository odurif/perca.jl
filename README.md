# PERCA

[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://odurif.gitlab.io/PERCA.jl/)

*PERCA.jl* is package providing tools for processing and plotting PERCA data.

## Installation
```julia-repl
(@1.9) pkg> add PERCA
```

## Reporting issues

[durif@kth.se](mailto:durif@kth.se)
